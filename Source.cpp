#include <string>
#include <vector>
#include <iostream>
#include <windows.h>
#include <fstream>

using namespace std;
#define MAX_PATH  2048
typedef int(__stdcall *f_funci)();

void pwd();
string rpwd();
void cd(const char* path);
void create(string fileName);
void ls();
int secret();
void executable();

int main()
{
	string what;
	cout << "what do you want to do? " << endl;
	cin >> what;
	while (true)
	{
		if (what == "pwd")
		{
			pwd();
		}
		else if (what == "cd")
		{
			cout << "enter path" << endl;
			char* path;
			cin >> path;
			cd(path);
		}
		else if (what == "create")
		{
			cout << "enter file name" << endl;
			string name;
			cin >> name;
			create(name);
		}
		else if (what == "ls")
		{
			ls();
		}
		else if (what == "secret")
		{
			if (secret() == 1)
			{
				cout << "sommething wrong" << endl;
			}
		}
	}
	system("pause");
	return 0;
}
 //this function print the current working dir path by getting the name
//and then checking the names path
void pwd()
{
	char buff[MAX_PATH];
	GetModuleFileName(NULL, buff, MAX_PATH); 
	string::size_type pos = string(buff).find_last_of("\\/");
	cout << "the path of current directory is: " << (string(buff).substr(0, pos)) << endl;
}
string rpwd()
{
	char buff[MAX_PATH];
	GetModuleFileName(NULL, buff, MAX_PATH);
	string::size_type pos = string(buff).find_last_of("\\/");
	return (string(buff).substr(0, pos));
}
//this function get path and changing current path to the given path
void cd(const char* path)
{
	if (SetCurrentDirectory(path) == 0)
	{
		throw("Error - wrong path");
	}
}

void create(string fileName)
{
	fstream f;
	f.open(fileName, fstream::in | fstream::out);
}

void ls()
{
	string folder = rpwd();
	vector<string> names;
	string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) 
			{
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	for (vector<string>::const_iterator i = names.begin(); i != names.end(); ++i)
	{
		cout << *i << ' ';
	}
}

int secret()
{
	HINSTANCE hGetProcIDDLL = LoadLibrary("C:\\Users\\User\\Desktop\\c++����\\Project1\\Project1\\Secret.dll");
	if (!hGetProcIDDLL)
	{
		cout << "could not load the dynamic library" << endl;	
		return 1;
	}

	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!funci)
	{
		cout << "could not locate the function" << endl;
		return 1;
	}
	cout << "TheAnswerToLifeTheUniverseAndEverything() returned " << funci() << endl;
}

void executable()
{

}